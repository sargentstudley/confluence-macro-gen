# Confluence Macro Gen

Confluence Macro Gen is a python utility that is used to generate python classes that represent Atlassian Confluence's macro objects, originally written by [Matthew Studley].

The purpose of this is to:

  - Create a python library for working with Confluence content in python, without remembering storage formats. 
  - Avoid the issue of having to maintain a python library of Confluence macro classes manually.  
  - Make Confluence easier to work with from the API in Python.

An example of what using python to access and create Confluence content *should* look like:

```python
import confluence.macros
myInfoMacro = macros.Info()
# Adds additional macros or text. 
myInfoMacro.children.add(macros.Paragraph("This is some info!")) 
#Use this string as the value of your content when posting to the REST API.
storageFormatString = str(myInfoMacro) 
```
### How It Works ###
The library works by using the Atlassian documentation as the "source of truth" for Macros. The macro generator is given the page ID of the Confluence docs "Working with Macros" page. It then uses the Confluence API to fetch the children pages (the macro documentation) in storage format. It looks for the code macro that contains the storage definition xml, and then uses that to create a python class. 

These classes are then printed out to a .py file, and can be tailored from there. In the future, serialization / pickling functions will be implemented that allow for the parsing of an entire page into macro objects!

### Usage:
TBD once code complete is reached. 


### Version
0.0.1

### Libraries Used

Currently only one library is needed for this to run properly:
*  [lxml] - Very powerful xml parsing library for Python. 


### Todo's
This is still very, very, *very* early in development. This is a rough list of things to do. 

- Add handling of user UUIDs in some macros - these aren't supported yet
- Serialization system for all classes. 
- Comments / Documentation for generated classes?
- Support for pages, sections, tables, etc.
- Usage documentation for the confluence-macro-gen library itself. 


### Contributing
Want to help out by contributing code to this library? You are more than welcome to do so! You can contribute in the following ways: 

 - **Write Code:** Make a pull request into this library. Code is always welcome!
 - **Bug reports:** If something doesn't work correctly, file a bug report. Details are appreciated!
 - **Discussion and ideas, feature requests:** What would you like to see this library support in the future? Comment, write an e-mail, etc. 
 
License
----
MIT


[lxml]:http://lxml.de/index.html
[Matthew Studley]:http://www.mattstudley.com