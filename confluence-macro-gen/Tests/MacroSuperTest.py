import unittest

from MacroSuper import Macro
import Macros

__author__ = 'Jack'
'''
Test module for loading the MacroSuper class and testing it out. Being used primarily for development.
'''


class testMacroSuper(unittest.TestCase):

    __statusMacroStorageString = '<ac:structured-macro ac:name="status">' \
                                 '  <ac:parameter ac:name="colour">Green</ac:parameter>' \
                                 '  <ac:parameter ac:name="title">On track</ac:parameter>' \
                                 '  <ac:parameter ac:name="subtle">true</ac:parameter>' \
                                 '</ac:structured-macro>'

    __originalParams = {"variable1": "variable-1",
                        "variable2": "variable2"}

    __storageName = "Test-Macro"

    def test_TranslateString(self):
        statusMacro = Macros.Status()
        statusMacro.fromstoragestring(self.__statusMacroStorageString)
        self.assertEqual(statusMacro.colour, 'Green')


if __name__ == '__main__':
    unittest.main()