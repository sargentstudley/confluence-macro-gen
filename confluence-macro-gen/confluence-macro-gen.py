__author__ = 'Sarge'

import json
import requests
import time
from lxml import etree


#TODO - Create generic serialization system that translates a populated object back into storage xml.

class ConfluenceMacroGen():
    confDocRootUrl = ""
    macroPageId = 0
    pythonFilename = ""
    pythonClassName = ""
    commentHeaderFile = ""

    def __init__(self, confDocUrl, pageId, filename, classname):
        self.confDocRootUrl = confDocUrl
        self.macroPageId = pageId
        self.pythonFilename = filename
        self.pythonClassName = classname

    def DoMacroBuild(self):
        '''
        Primary method. Calls other methods to build and output the macro file.
        :return:
        '''
        print("Step 1: Fetching all child pages from page ID: {}...".format(self.macroPageId))
        allMacroNodes = testClass.GetMasterListOfMacros()
        print("Step 2: Extracting storage format documentation from code blocks and generating class texts...")
        pythonClassText = testClass.BuildPythonClasses(allMacroNodes.values())
        print("Step 3: Fetching comment header text from file {}".format(self.commentHeaderFile))
        commentText = self.GetHeaderComment(self.commentHeaderFile)
        dateStampText = "# This file was generated on {}.".format(time.strftime("%c"))
        pythonClassText = ''.join([commentText, '\n', dateStampText, '\n', pythonClassText])

        print("Step 4: Writing the file.")
        testClass.MakeFile(pythonClassText)

    def GetMacroCodeFromPage(self,pageId):
        #Define the Url for the Confluence API to get content.
        targetUrl = "{}/rest/api/content/{}".format(self.confDocRootUrl, pageId)

        #Form, issue, and parse our REST call.
        querystrings = {'expand': 'body.storage'}
        response = requests.get(targetUrl, params=querystrings)
        jsonresult = json.loads(response.text)

        #Grab the storage body out of the json response.
        rawhtml = jsonresult['body']['storage']['value']

        #Parse the xml string into lxml.etree, taking a side-trip through a preparation method.
        parsedXml = etree.fromstring(self.__cleanStorageXml(rawhtml))

        #Pull the code block out of the page.
        codeDefinition = parsedXml.xpath('//ac:structured-macro[@ac:name="code"]/ac:plain-text-body', namespaces={'ac': 'confluenceac'})

        #Get the raw storage format of the code.
        if len(codeDefinition) == 1:
            return etree.fromstring(self.__cleanStorageXml(codeDefinition[0].text))
        elif len(codeDefinition) > 1:
            #We found multiples! We need some logic to explore this.
            for codeblock in codeDefinition:
                if "ac:structured-macro" in codeblock.text:
                    return etree.fromstring(self.__cleanStorageXml(codeblock.text))
        else:
            #Apparently there is no code block, so we're boned.
            return None

    def GetMasterListOfMacros(self):
        targeturl = "{}/rest/api/content/{}/child/page".format(self.confDocRootUrl, self.macroPageId)
        querystrings = {'limit': 100}
        response = requests.get(targeturl, params=querystrings)

        jsonresult = json.loads(response.text)
        childPageContent = {}


        for childpage in jsonresult['results']:
            print("Getting raw child content for {}...".format(childpage['title']))
            rawContent = self.GetMacroCodeFromPage(childpage['id'])
            childPageContent[childpage['id']] = rawContent
        print("...done!")
        return childPageContent

    def BuildPythonClasses(self, listOfStorageXml):
        '''
        Takes a list of etree nodes that represent storage xml definitions of Confluence Macros, and creates python
        classes from them.
        :param listOfStorageXml: The etree _element nodes to build macro classes from.
        :return: The string text of a whole bunch of macro classes - maybe you should output it to a file?
        '''

        pythonText = ""

        #Import the superclass into the module. Start the module string with this.
        pythonText = "from MacroSuper import Macro"

        for rootnode in listOfStorageXml:
            if rootnode is None or len(rootnode) == 0:
                #The root node is none (We didn't find a definition for this element, so we can't do anything.)
                continue

            macroNode = rootnode[0]  # Get the child of the root, which is the actual root of the macro.
            classString = self.__BuildMacroClass(macroNode)
            pythonText = ''.join([pythonText, '\n', classString, '\n'])  # Concat this all together.

        return pythonText

    def GetHeaderComment(self, filePath):

        with open(filePath, 'r') as commentFile:
            commentText = commentFile.read()
            return commentText

    def MakeFile(self, classText):
        '''
        Writes the python class text to a file.
        :param classText: The text of the macro text.
        '''

        print("Writing python class to disk...")
        with open(self.pythonFilename, 'w') as pythonFile:
            pythonFile.write(classText)

        print("Python Macro file compilation completed.")



    #Private methods for the class
    def __BuildMacroClass(self, macroNode):
        '''
        Builds the string representation of a python class that represents a confluence macro.
        :param macroNode: The lxml ac:structured-content node that is the actual root of a confluence macro.
        :return: A string class definition.
        '''

        #Serialization variables.
        pythonKeywords = ['class', 'type']
        className = macroNode.get('{confluenceac}name')
        paramDict = {}

        #Constructor / Init for superclass inheritance.
        constructorText = "def __init__(self):\n        super().__init__(self.__storageName, self.__originalParams)"

        print("Building {}...".format(className))

        storageName = className  #Copy the name of the element, so it is stored in the class. Use for serialization.
        classString = "class {}(Macro):".format(className.capitalize().replace("-", ""))

        #Add the storageName parameter first. This lets us remember the original storage name later.
        classString = ''.join([classString, '\n    ', "__storageName", ' = ', '"{}"'.format(storageName)])

        allParameters = macroNode.findall('ac:parameter', namespaces={'ac': 'confluenceac', 'rc': 'confluencerc'})

        #Now loop through each parameter and generate an attribute for each one. Most of these will be simple vars.
        #TODO: User attributes contain a UUID, so we need to find a way to enforce that eventually.
        #TODO: Handle empty parameter names that have stuff in them.
        for param in allParameters:
            paramName = param.get('{confluenceac}name')
            originalParamName = paramName
            warningMessage = ""  # Comment that we will append to the end of the line if we spot a problem.

            #Parameter name isn't defined, which means we need to go dig into the element to find whats going on.
            if paramName is None or paramName == '':
                paramName = "emptyParam"  # Start with 'emptyParam', so we can't get null / blank variable names.

                #Block of If statements (that should be abstracted out to a private method) to figure out the param.
                if len(param) > 0:
                    childNode = param[0]
                    if childNode.tag == '{confluenceri}user':
                        paramName = 'user'
                        warningMessage = "  # WARN: User parameter found. This isn't supported yet!"

                elif param.text != '' and not param.text.isdigit():
                    paramName = param.text
                    originalParamName = ""
                    warningMessage = "  # WARN: Ambiguous parameter."

            #Check to make sure we aren't using a keyword, like class!
            if paramName in pythonKeywords:
                paramName = "{}param".format(paramName)

            #Store the original parameter name in the dictionary.
            paramDict[paramName] = originalParamName

            #Add the parameter definition to the class text.
            classString = ''.join([classString, '\n    ', paramName.replace('-', ''), ' = ""'])

            if warningMessage != "":
                classString = ''.join([classString, warningMessage])

        #Look for a rich text body element, meaning that this macro can contain other macros.
        richTextBody = macroNode.find('ac:rich-text-body', namespaces={'ac': 'confluenceac', 'rc': 'confluencerc'})

        if richTextBody is not None:
            classString = ''.join([classString, '\n    ', 'children', ' = []'])

        #Look for a plain text body element and add it if required.
        plainTextBody = macroNode.find('ac:plain-text-body', namespaces={'ac': 'confluenceac', 'rc': 'confluencerc'})

        if plainTextBody is not None:
            classString = ''.join([classString, '\n    ', 'plainTextBody', ' = ""'])


        #Add the dictionary of original parameter names for de-serialization.
        if len(paramDict) > 0:
            dictionaryText = json.dumps(paramDict)
            classString = ''.join([classString, '\n    ', '__originalParams = ', dictionaryText])
        else:
            classString = ''.join([classString, '\n    ', '__originalParams = {}'])

        #Add the constructor to the end of the class.
        classString = ''.join([classString, '\n    ', constructorText])

        return classString

    def __cleanStorageXml(self, rawxmlstring):
        '''
        Adds namespace definitions and replaces entities with utf-8 counterparts, preparing a string to be parsed by
        lxml or similar library.
        :param rawXmlString: The string of the xml that needs to be cleaned.
        :return: The cleaned xml string.
        '''

        #We need to define the ac and ri namespaces in the storage format in order for lxml/etree to play nice.
        namespaced = '<root xmlns:ac="confluenceac" xmlns:ri="confluenceri">{}</root>'.format(rawxmlstring)

        #Now we need to scrub out the &tags.
        entities = [
            ('&nbsp;', u'\u00a0'),
            ('&acirc;', u'\u00e2'),
            ('&ndash;', u'\u2013'),
            ('&mdash;', u'\u2014'),
            ('&rsquo;', u'\u2019')
        ]

        for before, after in entities:
            namespaced = namespaced.replace(before, after)

        return namespaced


if __name__ == "__main__":
    testClass = ConfluenceMacroGen("http://confluence.atlassian.com", 139387,
                                   "Macros.py",
                                   "Macros")
    testClass.commentHeaderFile = "headercomment.txt"

    testClass.DoMacroBuild()




