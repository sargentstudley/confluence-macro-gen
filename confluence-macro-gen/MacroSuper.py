from lxml import etree
import inspect


class Macro():

    __storageName = ""
    __originalParams = {}

    #__namespaceDefinitions = {
    #    'ac': 'confluenceac',
    #    'rc': 'confluencerc'
    #}

    def __init__(self, storageName, originalParams):
        self.__storageName = storageName
        self.__originalParams = originalParams

        self.__namespaceDefinitions = {
            'ac': 'confluenceac',
            'rc': 'confluencerc'
        }

        #Define the custom confluence prefixes in the namespace.
        for key in self.__namespaceDefinitions.keys():
            etree.register_namespace(key, self.__namespaceDefinitions[key])

    def toxml(self):
        '''
        Serializes a Macro (and children, if applicable) into Storage XML format.
        :return: string - storage XML format.
        '''

        attributesList = self.__getAttributes()

        #Build the root.
        rootXml = etree.Element('{{{}}}structured-macro'.format(self.__namespaceDefinitions['ac']))
        rootXml.set("{{{}}}name".format(self.__namespaceDefinitions['ac']), self.__storageName)

        for attribKey in attributesList.keys():
            rootXml.append(self.__genXmlFromParameter(attribKey, attributesList[attribKey]))

        return rootXml

    def fromxml(self, storageXmlNode):

        macroName = storageXmlNode.get("ac:name")

        #Check and make sure we are parsing the right kind of macro.
        if macroName.lower() != self.__class__.__name__.lower():
            raise Exception("Incorrect Macro type for this class.")

        #Create a reversed dictionary from our storage dictionary.
        revdict = dict((v, k) for k, v in self.__originalParams.items())

        for param in storageXmlNode.findall('parameter'):
            paramname = revdict[param.get('ac:name')]
            paramvalue = param.text
            setattr(self, paramname, paramvalue)





    def tostoragestring(self):
        return etree.tostring(self.toxml())

    def fromstoragestring(self, storageXmlString):
        '''
        Takes storage xml as a string and populates the macro object based on the contents.
        :param storageXmlString: The storage xml as a string to parse.
        '''
        #Define the custom confluence prefixes in the namespace.
        for key in self.__namespaceDefinitions.keys():
            etree.register_namespace(key, self.__namespaceDefinitions[key])

        xmlNode = etree.HTML(storageXmlString)
        self.fromxml(xmlNode[0][0])

    def __getAttributes(self):
        '''
        Gets a list of all attributes that are relevant to serialize.
        :return: dictionary of parameters and their values.
        '''

        allParameters = inspect.getmembers(self)
        attributelist = {}
        for paramname, paramvalue in allParameters:
            if paramname.startswith('_'):
                continue

            if inspect.ismethod(getattr(self, paramname)):
                continue

            # What's left after the two tests above are attributes, so we should process them.
            attributelist[paramname] = paramvalue

        return attributelist

    def __genXmlFromParameter(self, name, value):
        '''
        Generates XML element based on parameter.
        :param name: The name of the parameter.
        :param value: The value of the parameter to write.
        :return: lxml.etree node representing the storage format of the parameter.
        '''

        # Handle special cases for the children container, etc...
        if name == "children":
            return self.__genChildrenXml()

        acNameSpace = "{{{}}}".format(self.__namespaceDefinitions['ac'])
        elementName = "{}parameter".format(acNameSpace)
        paramNode = etree.Element(elementName)
        paramNode.set("{}name".format(acNameSpace), self.__originalParams[name])
        paramNode.text = value

        return paramNode

    def __genChildrenXml(self):
        '''
        For each child macro that is contained in this macro, return it's xml.
        :return:
        '''

        richTextBodyNode = etree.Element("{{{}}}rich-text-body".format(self.__namespaceDefinitions['ac']))

        for childNode in getattr(self, "children"):
            richTextBodyNode.append(childNode.toxml)

        return richTextBodyNode





